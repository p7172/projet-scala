package undirected

/** Implementation of [[SimpleGraph]] using list of neighbors for each vertex
  * @param neighbors associative map providing set of neighbors for each vertex
  *                  Key must be defined for any vertex in graph : should an actual vertex have no neighbor, value is defined and is an empty set
  * @tparam V type for vertices
  */
case class SimpleGraphNeighborsImpl[V](neighbors : Map[V, Set[V]]) extends SimpleGraph[V] {

    /** @inheritdoc */
    val vertices : Set[V] = neighbors.keySet

    /** @inheritdoc */
    val edges : Set[Edge[V]] =
        (for(v <- vertices) yield neighbors.getOrElse(v, Set[V]()).map(x => Edge[V](v, x))).flatten

    /** @inheritdoc */
    def neighborsOf(v: V) : Option[Set[V]] =
        if (!(vertices contains v)) None else Some(neighbors(v))

    /** @inheritdoc */
    def + (v : V) : SimpleGraphNeighborsImpl[V] =
        SimpleGraphNeighborsImpl(neighbors ++ Map(v -> Set[V]()))

    /** @inheritdoc */
    def - (v : V) : SimpleGraphNeighborsImpl[V] = {
        val newVertices = (vertices-v).toSeq
        val newEdges = for (vn <- newVertices) yield neighbors getOrElse(vn, Set[V]()) filter (_ != v)
        val newNeighbors = (newVertices zip newEdges).toMap
        SimpleGraphNeighborsImpl(newNeighbors)
    }

    /** @inheritdoc */
    def +| (e: Edge[V]) : SimpleGraphNeighborsImpl[V] = {
      if(e._1!=e._2) { // Test si c'est une boucle
        if (vertices.contains(e._1) && vertices.contains(e._2)) { // Test si les deux sommets existent dans le graphe
          if (!edges.contains(e)) { // Test si la liaison existe déjà
            val vSeq = vertices.toSeq
            val newListNeighborsForEachVertex =
              for (v <- vSeq) yield
                if (v == e._1)
                  neighbors.getOrElse(v, Set[V]()) + e._2
                else if (v == e._2)
                  neighbors.getOrElse(v, Set[V]()) + e._1
                else
                  neighbors.getOrElse(v, Set[V]())

            SimpleGraphNeighborsImpl((vSeq zip newListNeighborsForEachVertex).toMap)
          }
          else
            this
        }
        else if (!vertices.contains(e._1) && !vertices.contains(e._2)) // Si aucun des deux sommets n'existe
          ((this + e._1) + e._2) +| e // On les ajoute au graphe et on ajoute la liaison
        else if (!vertices.contains(e._1)) // Si le premier sommet seulement n'existe pas
          (this + e._1) +| e // On l'ajoute au graphe et on ajoute la liaison
        else // Si le deuxième sommet seulement n'existe pas
          (this + e._2) +| e // On l'ajoute au graphe et on ajoute la liaison
      }
      else
        this
    }

    /** @inheritdoc */
    def -| (e: Edge[V]) : SimpleGraphNeighborsImpl[V] = {
        if(edges.contains(e)) {
            val vSeq = vertices.toSeq
            val newListNeighborsForEachVertex =
                for (v <- vSeq) yield
                    if (v == e._1)
                        neighbors.getOrElse(v, Set[V]()) - e._2
                    else if(v == e._2)
                        neighbors.getOrElse(v, Set[V]()) - e._1
                    else
                        neighbors.getOrElse(v, Set[V]())

            SimpleGraphNeighborsImpl((vSeq zip newListNeighborsForEachVertex).toMap)
        }
        else
            this
    }

    /** @inheritdoc */
    def withoutEdge : SimpleGraphNeighborsImpl[V] =
        SimpleGraphNeighborsImpl((vertices zip List.fill(vertices.size)(Set[V]())).toMap)

    /** @inheritdoc */
    def withAllEdges : SimpleGraphNeighborsImpl[V] = {
        val vSeq = vertices.toSeq
        val listNeighborsForEachVertex = for (v <- vSeq) yield vertices.filter(_ != v)
        SimpleGraphNeighborsImpl((vSeq zip listNeighborsForEachVertex).toMap)
    }
}
