package undirected

import sun.security.provider.certpath.AdjacencyList

/** Implementation of [[SimpleGraph]] using adjacency matrix
  *
  * @param vs sequence of vertices in the order they are used in adjacency matrix
  * @param adjacency adjacency matrix
  * @tparam V type for vertices
  */
case class SimpleGraphMatrixImpl[V](vs : Seq[V], adjacency : IndexedSeq[IndexedSeq[Int]]) extends SimpleGraph[V] {

    /** @inheritdoc */
    lazy val vertices : Set[V] = vs.toSet

    lazy val tailVs: Int = vs.length

    lazy val tailAdj: Int = adjacency.size

    /*val nbVertices = List.range(1,vertices.size+1)
    val fusion = (vertices zip nbVertices).toSeq*/
    /** @inheritdoc */
    lazy val edges : Set[Edge[V]] = {
        val a: Set[Edge[V]] = Set()
        for(i <- 0 to vs.length) yield
            for(j <- 0 to vs.length) yield
                if(adjacency(i)(j) == 1){
                    a ++ Set(vs(i), vs(j))
                }
        a
    }

    /** @inheritdoc */
    def neighborsOf(v : V) : Option[Set[V]] = {
        if (!(vertices contains v)) None else Some(vertices filter { edges contains Edge(v,_) })
    }

    /**retourne la nouvelle matrice d'adjacence avec le sommet ajouté initialisé à 0*/
    def returnNewMatrix(adjacency : IndexedSeq[IndexedSeq[Int]]) : IndexedSeq[IndexedSeq[Int]] =
        (for {i<- 0 until tailVs} yield {
            adjacency.updated(i,
                for { j<- tailVs-1 to i} yield {
                    adjacency.updated(j,0).asInstanceOf[Int]
                }
            )
        }).flatten
    /** retourne un nv seq de V avec l'ajout de v à vs */
    def newSeqV(v:V, vs: Seq[V]):Seq[V] = v +: vs

    /** @inheritdoc */
    def + (v : V) : SimpleGraphMatrixImpl[V] = {


        if (vertices contains v)
            SimpleGraphMatrixImpl(vs, adjacency)
        else
            SimpleGraphMatrixImpl(newSeqV(v,vs), returnNewMatrix(adjacency))
    }

    /** retourne la matrice réduite */
    def returnMatrixRed(v:V ,adjacency : IndexedSeq[IndexedSeq[Int]]) : IndexedSeq[IndexedSeq[Int]] ={
        val l = vs.indexOf(v)
        val nal = adjacency.dropRight(vs.size - l)
        val nal2 = adjacency.drop(l + 1)
        val na = nal ++ nal2

        val left = for (i <- 0 until vs.size - 1) yield {
            na(i).dropRight(vs.size - l)
        }
        val right = for (i <- 0 until vs.size - 1) yield {
            na(i).drop(l + 1)
        }
        val end = for (i <-0 until vs.size) yield left(i) ++ right(i)
        end
    }
    /** retourne la nouvelle sequence avec le vertex enlevé */
    def newSeqVRed(v:V, vs: Seq[V]):Seq[V] = vs.filter(_!= v)
    /** @inheritdoc */
    def - (v : V) : SimpleGraphMatrixImpl[V] = SimpleGraphMatrixImpl(newSeqVRed(v,vs),returnMatrixRed(v, adjacency))

    /** @inheritdoc */
    def +| (e : Edge[V]) : SimpleGraphMatrixImpl[V] = {
        if (edges.contains(e)) {
            SimpleGraphMatrixImpl(vs, adjacency)
        }
        edges ++ Set(e)
        adjacency(vs.indexOf(e._1)).updated(vs.indexOf(e._2),1)
        SimpleGraphMatrixImpl(vs, adjacency)
    }

    /** @inheritdoc */
    def -| (e : Edge[V]) : SimpleGraphMatrixImpl[V] = {
        if (edges.contains(e)) {
            SimpleGraphMatrixImpl(vs, adjacency)
        }
        edges -- Set(e)
        adjacency(vs.indexOf(e._1)).updated(vs.indexOf(e._2),0)
        SimpleGraphMatrixImpl(vs, adjacency)
    }



    /**retourne la nouvelle matrice d'adjacence avec 0 edge*/
    def returnMatrixZero(adjacency : IndexedSeq[IndexedSeq[Int]]) : IndexedSeq[IndexedSeq[Int]] =
        (for {i<- 0 to tailVs} yield {
            adjacency.updated(i,
                for { j<- 0 to tailVs} yield {
                    adjacency.updated(j,0).asInstanceOf[Int]
                }
            )
        }).flatten

    /** @inheritdoc */
    def withoutEdge : SimpleGraphMatrixImpl[V] = SimpleGraphMatrixImpl(vs, returnMatrixZero(adjacency))

    /**retourne la nouvelle matrice d'adjacence avec tous les edges possible edge*/
    def returnMatrixOne(adjacency : IndexedSeq[IndexedSeq[Int]]) : IndexedSeq[IndexedSeq[Int]] =
        (for {i<- 0 to tailVs} yield {
            adjacency.updated(i,
                for { j<- 0 to tailVs} yield {
                    adjacency.updated(j,1).asInstanceOf[Int]
                }
            )
        }).flatten

    /** @inheritdoc */
    def withAllEdges : SimpleGraphMatrixImpl[V] = SimpleGraphMatrixImpl(vs, returnMatrixOne(adjacency))
  }



