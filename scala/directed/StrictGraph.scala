package directed

import scala.annotation.tailrec

/** Trait for a directed ''and strict'' graph, i.e. without loop nor parallel arcs */
trait StrictGraph[V] {
    /* QUERY METHODS */

    /** The set of all vertices of the graph */
    val vertices: Set[V]

    /** The set of all     arcs of the graph */
    val arcs: Set[Arc[V]]

    /** The set of all vertices with arcs incoming from input vertex
      *
      * @param v vertex
      * @return [[None]] if `v` is not an actual vertex, the set of all successors of `v` otherwise
      */
    def successorsOf(v: V): Option[Set[V]]

    /** The number of incoming arcs to input vertex
      *
      * @param v vertex
      * @return [[None]] if `v` is not an actual vertex, the inner degree of `v` otherwise
      */
    def inDegreeOf(v: V): Option[Int] =
        if (!(vertices contains v)) None
        else Some(vertices count {
            arcs contains Arc(_, v)
        })

    /** The number of outcoming arcs to input vertex
      *
      * @param v vertex
      * @return [[None]] if `v` is not an actual vertex, the outer degree of `v` otherwise
      */
    def outDegreeOf(v: V): Option[Int] =
        if (!(vertices contains v)) None
        else Some(vertices count {
            arcs contains Arc(v, _)
        })

    /** The number of adjacent vertices to input vertex
      *
      * @param v vertex
      * @return [[None]] if `v` is not an actual vertex, the degree of `v` otherwise
      */
    def degreeOf(v: V): Option[Int] =
        if (!(vertices contains v)) None
        else Some(inDegreeOf(v).getOrElse(0) + outDegreeOf(v).getOrElse(0))

    /* VERTEX OPERATIONS */

    /** Add vertex to graph
      *
      * @param v new vertex
      * @return the graph with new vertex `v`
      *         if `v` is an actual vertex of graph, return input graph
      */
    def +(v: V): StrictGraph[V]

    /** Remove vertex from graph
      *
      * @param v new vertex
      * @return the graph without vertex `v`
      *         if `v` is not an actual vertex of graph, return input graph
      */
    def -(v: V): StrictGraph[V]

    /* ARC OPERATIONS */

    /** Add arc to graph (also add arc ends as new vertices if necessary)
      *
      * @param a new arc
      * @return the graph with new arc `e`
      *         if `e` is an actual arc of graph, return input graph
      */
    def +|(a: Arc[V]): StrictGraph[V]

    /** Remove arc from graph (does NOT remove ends)
      *
      * @param a new arc
      * @return the graph without arc `e`
      *         if `e` is not an actual arc of graph, return input graph
      */
    def -|(a: Arc[V]): StrictGraph[V]

    /** Remove all arcs from graph but keep same vertices
      *
      * @return graph with same vertices without any arc
      */
    def withoutArc: StrictGraph[V]

    /** Add all possible arc with same vertices
      *
      * @return graph with same vertices and all possible arcs
      */
    def withAllArcs: StrictGraph[V]

    /* SEARCH METHODS */

    /** A topological order of the vertex set (if exists) */
    lazy val topologicalOrder: Option[Seq[V]] = {
        if (vertices.isEmpty || hasCircuit(vertices.head, Seq[V](vertices.head), vertices--Set(vertices.head), Set[V]())) None
        else Some(topologicalOrderDfs(vertices.head, Seq[V](), Seq[V](vertices.head))) // Use of toSeq to use sortWith method
    }

    @tailrec
    final def hasCircuit(actualVertex: V, visitInProgress: Seq[V], remainingToVisit: Set[V], visited : Set[V]) : Boolean = {
        val successorsUnvisited = successorsOf(actualVertex).getOrElse(Set[V]())--visited

        if(successorsUnvisited.isEmpty) // If it has no successors unvisited
            if((visitInProgress.toSet--Set(actualVertex)).isEmpty) // If the actual vertex is the only one left in the visitInProgress list
                if(remainingToVisit.isEmpty) // If there is no vertex left to visit
                    false
                else {
                    val nextVertex = remainingToVisit.head
                    hasCircuit(nextVertex, Seq[V](nextVertex), remainingToVisit--Set(nextVertex), visited++Set(actualVertex))
                }
            else {
                hasCircuit(visitInProgress.filter(_!=actualVertex).head, visitInProgress.filter(_!=actualVertex), remainingToVisit--Set(actualVertex), visited++Set(actualVertex))
            }
        else if(successorsUnvisited.exists(visitInProgress.toSet)) //Test if in the successors unvisited there is a vertex that is currently being visited
            true
        else {
            val nextVertex = successorsUnvisited.head
            hasCircuit(nextVertex, Seq(nextVertex)++visitInProgress, remainingToVisit--Set(nextVertex), visited)
        }
    }

    @tailrec
    final def topologicalOrderDfs(vertex : V, topologicalList : Seq[V], queue : Seq[V]) : Seq[V] = {
        val unvisitedSuccessors = successorsOf(vertex).getOrElse(Set[V]())--topologicalList

        if(unvisitedSuccessors.isEmpty) { // It has no unvisited successors
            val newQueue = queue.filter(_!=vertex)//queue.takeRight(queue.size-1) // We remove the actual vertex from the queue
            val newTopologicalList = Seq(vertex)++topologicalList
            if(vertices.size==newTopologicalList.size)
                newTopologicalList
            else if(newQueue.nonEmpty)
                topologicalOrderDfs(newQueue.head, newTopologicalList, newQueue)
            else
                topologicalOrderDfs((vertices--newTopologicalList).head, newTopologicalList, Seq[V]((vertices--newTopologicalList).head))
        }
        else
            topologicalOrderDfs(unvisitedSuccessors.head, topologicalList, (unvisitedSuccessors--queue.toSet).toSeq++queue)
    }

    /* VALUATED GRAPH METHODS */

    /** Computes a shortest path between two vertices
      * @param valuation valuation of graph
      * @param start origin      of path
      * @param end   destination of path
      * @return [[None]] if there is no path from `start` to `end`, the shortest path and its valuation otherwise
      */
    def shortestPath(valuation : Map[Arc[V], Double])(start : V, end : V) : Option[(Seq[V], Double)] = {
        val mapDistances = (for (i <- 0 until vertices.size) yield {vertices.toSeq.apply(i)->Double.MaxValue}).toMap
        val parents=List.fill(vertices.size)(start)
        val usedDistances=Map.empty[V, Double]
        val unvisitedVertices=vertices.toSeq
        val res = (Dijkstra(valuation,unvisitedVertices,mapDistances+(start->0.0), usedDistances, parents, (Seq[V](start), 0.0))(start,end)).get

        val pathToEnd = Seq[V](end)
        Some(pathFromParents(res._1, pathToEnd, end)(start).reverse,res._2)
    }

    @tailrec
    final def Dijkstra(valuation: Map[Arc[V], Double], unvisitedVertices:Seq[V], distances: Map[V,Double], usedDistances: Map[V,Double], parents: Seq[V], res: (Seq[V],Double))(start : V, end:V) : Option[(Seq[V], Double)] = {
        if(!unvisitedVertices.contains(end)) {
            Some(parents,res._2 + distances(end))
        }
        else {
            val newDistances=distances--usedDistances.keys
            val distanceMin = newDistances.values.min
            val vertexWithSmallestDistance = newDistances.find(_._2==distanceMin).map(_._1).get
            val newUnvisitedVertices = unvisitedVertices.filter(_!=vertexWithSmallestDistance)
            if (successorsOf(vertexWithSmallestDistance).isDefined) {
                val successorsOfVertex = successorsOf(vertexWithSmallestDistance).get
                val tuple = newDistance(valuation, vertexWithSmallestDistance, successorsOfVertex.toSeq, parents, distances)
                val newDistances2 = tuple._1
                val newParents = tuple._2

                val newUsedDistances=usedDistances+(vertexWithSmallestDistance->newDistances2(vertexWithSmallestDistance))
                Dijkstra(valuation, newUnvisitedVertices, newDistances2,newUsedDistances, newParents,  res)(vertexWithSmallestDistance, end)
            }
            else {
                val newUsedDistances = usedDistances + (start -> distances(start))
                Dijkstra(valuation, newUnvisitedVertices, distances, newUsedDistances, parents, res)(vertexWithSmallestDistance, end)
            }
        }

    }

    @tailrec
    final def newDistance(valuation: Map[Arc[V], Double], parent : V, successors : Seq[V], parents : Seq[V], distances : Map[V,Double]) : (Map[V,Double],Seq[V]) = {

        if(successors.nonEmpty) {
            if (distances(successors.head) > distances(parent) + valuation(Arc(parent, successors.head))) {
                val par = parents.updated(vertices.toSeq.indexOf(successors.head), parent)
                val dist = distances+(successors.head->(distances(parent) + valuation(Arc(parent, successors.head))))
                newDistance(valuation, parent, successors.tail, par, dist)
            }
            else {
                newDistance(valuation, parent, successors.tail, parents, distances)
            }
        }
        else (distances,parents)

    }

    @tailrec
    final def pathFromParents(parents : Seq[V], pathToEnd : Seq[V], chosenVertex : V)(start: V) : Seq[V] = {
        //sert à récuperer la seq à partir des parents (on prend le parent de end, etc jusqu'à start)
        if(chosenVertex==start){
            pathToEnd
        }
        else{
            val newPath = pathToEnd :+ parents(vertices.toSeq.indexOf(chosenVertex))
            val newChosen = parents(vertices.toSeq.indexOf(chosenVertex))
            pathFromParents(parents, newPath, newChosen)(start)
        }
    }

    /* toString-LIKE METHODS */

    /** @inheritdoc */
    override lazy val toString : String = s"({${vertices mkString ", "}}, {${arcs mkString ", "}})"

    /** Graph representation in DOT language */
    lazy val toDOTString : String = {
        "strict graph {\n" +
        "    // Edges\n" +
        (arcs foldLeft "    ") { _ + _.toDOTString + "\n    " } + "\n" +
        "    // Vertices\n" +
        vertices.mkString("    ", "\n    ", "\n") +
        "  }\n"
      }

  }
