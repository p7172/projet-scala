package directed

import scala.None

/** Implementation of [[StrictGraph]] using adjacency matrix
  * @param vs sequence of vertices in the order they are used in adjacency matrix
  * @param adjacency adjacency matrix
  * @tparam V type for vertices
  */
case class StrictGraphMatrixImpl[V](vs : Seq[V], adjacency : IndexedSeq[IndexedSeq[Int]]) extends StrictGraph[V] {

    /** @inheritdoc */
    lazy val vertices : Set[V] = vs.toSet

    /** @inheritdoc */
    lazy val arcs : Set[Arc[V]] = {
        val a: Set[Arc[V]] = Set()
        for(i <- 0 to vs.length) yield
            for(j <- 0 to vs.length) yield
                if(adjacency(i)(j) == 1){
                    a ++ Set(vs(i), vs(j))
                }
        a
    }

    lazy val tailVs: Int = vs.length

    /** @inheritdoc */
    def successorsOf(v : V) : Option[Set[V]] = {
        if(!(vertices contains v))
            None
        else {
            val successors: Set[V] = Set()
            for(s <-vs if arcs contains Arc[V](v,s)) yield successors ++ Set(s)
            Some(successors)
        }
    }

    /** Add vertex to graph
     * @param v new vertex
     * @return the graph with new vertex `v`
     *         if `v` is an actual vertex of graph, return input graph
     */
    /** @inheritdoc */
    /*def + (v : V) : StrictGraphMatrixImpl[V] = {
        if (vertices.contains(v)) {
            StrictGraphMatrixImpl(vs, adjacency)
        }
        vertices ++ Set(v)
        StrictGraphMatrixImpl(vs, adjacency)
    }*/

    /**retourne la nouvelle matrice d'adjacence avec le sommet ajouté initialisé à 0*/
    def returnNewMatrixPlus(adjacency : IndexedSeq[IndexedSeq[Int]]) : IndexedSeq[IndexedSeq[Int]] =
        (for {i<- 0 until tailVs} yield {
            adjacency.updated(i,
                for { j<- tailVs-1 to i} yield {
                    adjacency.updated(j,0).asInstanceOf[Int]
                }
            )
        }).flatten

    /** retourne un nv seq de V avec l'ajout de v à vs */
    def newSeqVPlus(v:V, vs: Seq[V]):Seq[V] = v +: vs

    def + (v : V) : StrictGraphMatrixImpl[V] = {
        if (vertices contains v)
            StrictGraphMatrixImpl(vs, adjacency)
        else
            StrictGraphMatrixImpl(newSeqVPlus(v,vs), returnNewMatrixPlus(adjacency))
    }

    /** Remove vertex from graph
     * @param v new vertex
     * @return the graph without vertex `v`
     *         if `v` is not an actual vertex of graph, return input graph
     */
    /** @inheritdoc */

    /* def - (v : V) : StrictGraphMatrixImpl[V] = {
         if (vertices.contains(v)) {
             StrictGraphMatrixImpl(vs, adjacency)
         }
         vertices -- Set(v)
         StrictGraphMatrixImpl(vs, adjacency)
     }*/

    /**retourne la nouvelle matrice d'adjacence sans le sommet v enlevé*/
    def returnNewMatrixMinus(adjacency : IndexedSeq[IndexedSeq[Int]], v:V) : IndexedSeq[IndexedSeq[Int]] = {
        val l = vs.indexOf(v)
        val nal = adjacency.dropRight(vs.size - l)
        val nal2 = adjacency.drop(l + 1)
        val na = nal ++ nal2

        val left = for (i <- 0 until vs.size - 1) yield {
            na(i).dropRight(vs.size - l)
        }
        val right = for (i <- 0 until vs.size - 1) yield {
            na(i).drop(l + 1)
        }
        val end = for (i <- left.indices) yield left(i) ++ right(i)
        end
    }

    /** retourne un nv seq de V avec l'ajout de v à vs */
    def newSeqVMinus(v:V, vs: Seq[V]):Seq[V] = vs.diff(Seq(v))

    def - (v : V) : StrictGraphMatrixImpl[V] = {
        if (!(vertices contains v)) StrictGraphMatrixImpl(vs, adjacency)
        else {
            StrictGraphMatrixImpl(newSeqVMinus(v,vs), returnNewMatrixMinus(adjacency, v))
        }
    }


    /** Add arc to graph (also add arc ends as new vertices if necessary)
     * @param a new arc
     * @return the graph with new arc `e`
     *         if `e` is an actual arc of graph, return input graph
     */
    /** @inheritdoc */
    def +| (e : Arc[V]) : StrictGraphMatrixImpl[V] = {
        if (arcs.contains(e)) {
            StrictGraphMatrixImpl(vs, adjacency)
        }
        arcs ++ Set(e)
        adjacency(vs.indexOf(e._1)).updated(vs.indexOf(e._2),1)
        StrictGraphMatrixImpl(vs, adjacency)
    }


    /** Remove arc from graph (does NOT remove ends)
     * @param a new arc
     * @return the graph without arc `e`
     *         if `e` is not an actual arc of graph, return input graph
     */
    /** @inheritdoc */
    def -| (e : Arc[V]) : StrictGraphMatrixImpl[V] = {
        if (arcs.contains(e)) {
            StrictGraphMatrixImpl(vs, adjacency)
        }
        arcs -- Set(e)
        adjacency(vs.indexOf(e._1)).updated(vs.indexOf(e._2),0)
        StrictGraphMatrixImpl(vs, adjacency)
    }

    /** Remove all arcs from graph but keep same vertices
     * @return graph with same vertices without any arc
     */
    /** @inheritdoc */
    def withoutArc : StrictGraphMatrixImpl[V] = {
        for (i <- 0 to vs.length) yield
            for(j <- 0 to vs.length) yield adjacency(i).updated(j,0)
        StrictGraphMatrixImpl(vs, adjacency)
    }
    /**retourne la nouvelle matrice d'adjacence avec 0 edge*/
    def returnMatrixZero(adjacency : IndexedSeq[IndexedSeq[Int]]) : IndexedSeq[IndexedSeq[Int]] =
        (for {i<- 0 to tailVs} yield {
            adjacency.updated(i,
                for { j<- 0 to tailVs} yield {
                    adjacency.updated(j,0).asInstanceOf[Int]
                }
            )
        }).flatten

    /** @inheritdoc */
    def withoutEdge : StrictGraphMatrixImpl[V] = StrictGraphMatrixImpl(vs, returnMatrixZero(adjacency))

    /** Add all possible arc with same vertices
     * @return graph with same vertices and all possible arcs
     */
    /** @inheritdoc */
    /*def withAllArcs : StrictGraphMatrixImpl[V] = {
        for (i <- 0 to vs.length) yield
            for(j <- 0 to vs.length) yield (adjacency(i)).updated(j,1)
        StrictGraphMatrixImpl(vs, adjacency)
    }*/

    /** @inheritdoc */
    def withAllArcs : StrictGraphMatrixImpl[V] = StrictGraphMatrixImpl(vs, returnMatrixOne(adjacency))

    /**retourne la nouvelle matrice d'adjacence avec tous les edges possible edge*/
    def returnMatrixOne(adjacency : IndexedSeq[IndexedSeq[Int]]) : IndexedSeq[IndexedSeq[Int]] =
        (for {i<- 0 to tailVs} yield {
            adjacency.updated(i,
                for { j<- 0 to tailVs} yield {
                    adjacency.updated(j,1).asInstanceOf[Int]
                }
            )
        }).flatten
}

