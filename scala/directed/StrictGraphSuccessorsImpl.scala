package directed

/** Implementation of [[StrictGraph]] using list of successors for each vertex
  * @param successors associative map providing set of successors for each vertex
  *                   Key must be defined for any vertex in graph : should an actual vertex have no neighbor, value is defined and is an empty set
  * @tparam V type for vertices
  */
case class StrictGraphSuccessorsImpl[V](successors : Map[V, Set[V]]) extends StrictGraph[V] {

    /** @inheritdoc */
    val vertices: Set[V] =
        successors.keySet

    /** @inheritdoc */
    val arcs: Set[Arc[V]] =
        (for(v <- vertices) yield successors.getOrElse(v, Set[V]()).map(x => Arc[V](v, x))).flatten

    /** @inheritdoc */
    def successorsOf(v: V): Option[Set[V]] =
        if (!(vertices contains v)) None else Some(successors(v))

    /** @inheritdoc */
    def +(v: V): StrictGraphSuccessorsImpl[V] =
        StrictGraphSuccessorsImpl(successors ++ Map(v -> Set[V]()))

    /** @inheritdoc */
    def -(v: V): StrictGraphSuccessorsImpl[V] = {
        val newVertices = (vertices-v).toSeq
        val newArcs = for (vn <- newVertices) yield successors getOrElse(vn, Set[V]()) filter (_ != v)
        val newSuccessors = (newVertices zip newArcs).toMap
        StrictGraphSuccessorsImpl(newSuccessors)
    }

    /** @inheritdoc */
    def +|(e: Arc[V]): StrictGraphSuccessorsImpl[V] = {
      if(e._1!=e._2) { // Test si c'est une boucle
        if (vertices.contains(e._1) && vertices.contains(e._2)) { // Test si les deux sommets existent dans le graphe
          if (!arcs.contains(e)) { // Test si l'arc existe déjà
            val vSeq = vertices.toSeq
            val newListSuccessorsForEachVertex =
              for (v <- vSeq) yield
                if (v == e._1)
                  successors.getOrElse(v, Set[V]()) + e._2
                else
                  successors.getOrElse(v, Set[V]())

            StrictGraphSuccessorsImpl((vSeq zip newListSuccessorsForEachVertex).toMap)
          }
          else
            this
        }
        else if (!vertices.contains(e._1) && !vertices.contains(e._2)) // Si aucun des deux sommets n'existe
          ((this + e._1) + e._2) +| e // On les ajoute au graphe et on ajoute la liaison
        else if (!vertices.contains(e._1)) // Si le deuxième sommet seulement n'existe pas
          (this + e._1) +| e // On l'ajoute au graphe et on ajoute la liaison
        else // Si le premier sommet seulement n'existe pas
          (this + e._2) +| e // On l'ajoute au graphe et on ajoute la liaison
      }
      else
        this
    }

    /** @inheritdoc */
    def -|(e: Arc[V]): StrictGraphSuccessorsImpl[V] = {
        if(arcs.contains(e)) { // Test si l'arc existe
            val vSeq = vertices.toSeq
            val newListSuccessorsForEachVertex =
                for (v <- vSeq) yield
                    if (v == e._1)
                        successors.getOrElse(v, Set[V]()) - e._2
                    else
                        successors.getOrElse(v, Set[V]())

            StrictGraphSuccessorsImpl((vSeq zip newListSuccessorsForEachVertex).toMap)
        }
        else
            this
    }

    /** @inheritdoc */
    def withoutArc: StrictGraphSuccessorsImpl[V] =
        StrictGraphSuccessorsImpl((vertices zip List.fill(vertices.size)(Set[V]())).toMap)

    /** @inheritdoc */
    def withAllArcs: StrictGraphSuccessorsImpl[V] = {
        val vSeq = vertices.toSeq
        val listSuccessorsForEachVertex = for (v <- vSeq) yield vertices.filter(_ != v)
        StrictGraphSuccessorsImpl((vSeq zip listSuccessorsForEachVertex).toMap)
    }
}